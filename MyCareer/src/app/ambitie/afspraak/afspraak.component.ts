import { Component, OnInit, Input } from "@angular/core";
import { Afspraak } from "src/app/models/afspraak.model";

@Component({
  selector: "app-afspraak",
  templateUrl: "./afspraak.component.html",
  styleUrls: ["./afspraak.component.css"]
})
export class AfspraakComponent implements OnInit {
  @Input() public afspraak: Afspraak;

  constructor() {}

  ngOnInit(): void {}
}
