import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "./../material/material.module";

import { AmbitieComponent } from "./ambitie/ambitie.component";
import { DoelstellingComponent } from "./doelstelling/doelstelling.component";
import { AfspraakComponent } from "./afspraak/afspraak.component";

@NgModule({
  declarations: [AmbitieComponent, DoelstellingComponent, AfspraakComponent],
  imports: [CommonModule, MaterialModule],
  exports: [AmbitieComponent]
})
export class AmbitieModule {}
