import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmbitieComponent } from './ambitie.component';

describe('AmbitieComponent', () => {
  let component: AmbitieComponent;
  let fixture: ComponentFixture<AmbitieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmbitieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmbitieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
