import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/data.service";
import { Observable } from "rxjs";
import { Ambitie } from "src/app/models/ambitie.model";

@Component({
  selector: "app-ambitie",
  templateUrl: "./ambitie.component.html",
  styleUrls: ["./ambitie.component.css"]
})
export class AmbitieComponent implements OnInit {
  public ambities$: Observable<Ambitie[]>;

  constructor(private _dataService: DataService) {}

  ngOnInit(): void {
    this.ambities$ = this._dataService.ambities$;
  }
}
