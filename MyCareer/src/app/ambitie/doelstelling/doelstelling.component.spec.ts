import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoelstellingComponent } from './doelstelling.component';

describe('DoelstellingComponent', () => {
  let component: DoelstellingComponent;
  let fixture: ComponentFixture<DoelstellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoelstellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoelstellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
