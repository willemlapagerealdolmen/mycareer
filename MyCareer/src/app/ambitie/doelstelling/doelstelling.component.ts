import { Component, OnInit, Input } from "@angular/core";
import { Doelstelling } from "src/app/models/doelstelling.model";

@Component({
  selector: "app-doelstelling",
  templateUrl: "./doelstelling.component.html",
  styleUrls: ["./doelstelling.component.css"]
})
export class DoelstellingComponent implements OnInit {
  @Input() doelstelling: Doelstelling;

  constructor() {}

  ngOnInit(): void {}
}
