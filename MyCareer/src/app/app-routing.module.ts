import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { AmbitieComponent } from "./ambitie/ambitie/ambitie.component";
import { LoginComponent } from "./user/login/login.component";
import { RegisterComponent } from "./user/register/register.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { AuthGuard } from "./user/auth.guard";
import { AmbitieModule } from "./ambitie/ambitie.module";
import { CompetentieComponent } from "./competentie/competentie/competentie.component";
import { ProfielComponent } from "./profiel/profiel/profiel.component";

const routes: Routes = [
  //{ path: 'Drank/Historiek', component: OverzichtComponent, canActivate: [AuthGuard] },
  {
    path: "Profiel",
    component: ProfielComponent /*, canActivate: [AuthGuard]*/
  },
  { path: "Login", component: LoginComponent },
  { path: "Registreer", component: RegisterComponent },
  { path: "", redirectTo: "Login", pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    AmbitieModule
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
