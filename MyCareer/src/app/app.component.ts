import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { Ambitie } from "./models/ambitie.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "MyCareer";

  get ambities$(): Observable<Ambitie> {
    return null;
  }
}
