import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { AmbitieModule } from "./ambitie/ambitie.module";
import { MaterialModule } from "./material/material.module";
import { HttpClientModule } from "@angular/common/http";
import { UserModule } from "./user/user.module";
import { httpInterceptorProviders } from "./http-interceptors";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { RouterModule } from "@angular/router";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { AppRoutingModule } from "./app-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { CompetentieModule } from "./competentie/competentie.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ProfielModule } from "./profiel/profiel.module";
import { DialogModule } from "./dialog/dialog.module";
import { TaalDialogComponent } from "./dialog/taal-dialog/taal-dialog.component";
import { MatDialogModule } from "@angular/material/dialog";

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, MainNavComponent],
  entryComponents: [TaalDialogComponent],
  imports: [
    BrowserModule,
    AmbitieModule,
    MaterialModule,
    HttpClientModule,
    UserModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CompetentieModule,
    ProfielModule,
    DialogModule,
    MatDialogModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {}
