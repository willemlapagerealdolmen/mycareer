import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../material/material.module";
import {
  CompetentieComponent,
  TaalPipe,
  TechnischeVaardighedenPipe,
  SocialeVaardighedenPipe
} from "./competentie/competentie.component";
import { OpleidingComponent } from "./opleiding/opleiding.component";
import { EventComponent } from "./event/event.component";
import { ProjectComponent } from "./project/project.component";
import { FunctieComponent } from "./functie/functie.component";
import { VaardigheidComponent } from "./vaardigheid/vaardigheid.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TaalDialogComponent } from "../dialog/taal-dialog/taal-dialog.component";

@NgModule({
  declarations: [
    CompetentieComponent,
    OpleidingComponent,
    EventComponent,
    ProjectComponent,
    FunctieComponent,
    VaardigheidComponent,
    TaalPipe,
    TechnischeVaardighedenPipe,
    SocialeVaardighedenPipe
  ],
  imports: [CommonModule, MaterialModule, NgbModule],
  exports: [CompetentieComponent]
})
export class CompetentieModule {}
