import {
  Component,
  OnInit,
  Pipe,
  PipeTransform,
  Input,
  Inject
} from "@angular/core";
import { Observable } from "rxjs";
import { Opleiding } from "src/app/models/opleiding.model";
import { DataService } from "src/app/data.service";
import { Event } from "src/app/models/event.model";
import { Project } from "src/app/models/project.model";
import { Functie } from "src/app/models/functie.model";
import { Vaardigheid } from "src/app/models/vaardigheid.model";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { TaalDialogComponent } from "src/app/dialog/taal-dialog/taal-dialog.component";

@Component({
  selector: "app-competentie",
  templateUrl: "./competentie.component.html",
  styleUrls: ["./competentie.component.css"]
})
export class CompetentieComponent implements OnInit {
  public opleidingen$: Observable<Opleiding[]>;
  public events$: Observable<Event[]>;
  public projecten$: Observable<Project[]>;
  public functies$: Observable<Functie[]>;
  public vaardigheden$: Observable<Vaardigheid[]>;
  @Input() isAmbitie: boolean;
  @Input() isEdit: boolean;

  taal: String;
  beheersing: String;

  constructor(private _dataService: DataService, private dialog: MatDialog) {}

  openTalenDialog(): void {
    const dialogRef = this.dialog.open(TaalDialogComponent, {
      width: "350px",
      data: { taal: this.taal, beheersing: this.beheersing, test: "test" }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  ngOnInit(): void {
    this.opleidingen$ = this._dataService.opleidingen$(this.isAmbitie);
    this.events$ = this._dataService.events$(this.isAmbitie);
    this.projecten$ = this._dataService.projecten$(this.isAmbitie);
    this.functies$ = this._dataService.functies$(this.isAmbitie);
    this.vaardigheden$ = this._dataService.vaardigheden$(this.isAmbitie);
  }
}

@Pipe({ name: "talen" })
export class TaalPipe implements PipeTransform {
  transform(alleVaardigheden: Vaardigheid[]) {
    return alleVaardigheden.filter(vaardigheid => vaardigheid.type == "TAAL");
  }
}

@Pipe({ name: "socialeVaardigheden" })
export class SocialeVaardighedenPipe implements PipeTransform {
  transform(alleVaardigheden: Vaardigheid[]) {
    return alleVaardigheden.filter(
      vaardigheid => vaardigheid.type == "SOCIAAL"
    );
  }
}

@Pipe({ name: "technischeVaardigheden" })
export class TechnischeVaardighedenPipe implements PipeTransform {
  transform(alleVaardigheden: Vaardigheid[]) {
    return alleVaardigheden.filter(
      vaardigheid => vaardigheid.type == "TECHNISCH"
    );
  }
}
