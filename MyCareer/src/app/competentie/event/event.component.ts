import { Component, OnInit, Input } from "@angular/core";
import { Event } from "src/app/models/event.model";

@Component({
  selector: "app-event",
  templateUrl: "./event.component.html",
  styleUrls: ["./event.component.css"]
})
export class EventComponent implements OnInit {
  constructor() {}
  @Input() event: Event;

  ngOnInit(): void {}
}
