import { Component, OnInit, Input } from "@angular/core";
import { Functie } from "src/app/models/functie.model";

@Component({
  selector: "app-functie",
  templateUrl: "./functie.component.html",
  styleUrls: ["./functie.component.css"]
})
export class FunctieComponent implements OnInit {
  constructor() {}
  @Input() functie: Functie;

  ngOnInit(): void {}
}
