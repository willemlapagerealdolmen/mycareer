import { Component, OnInit, Input } from "@angular/core";
import { Opleiding } from "src/app/models/opleiding.model";

@Component({
  selector: "app-opleiding",
  templateUrl: "./opleiding.component.html",
  styleUrls: ["./opleiding.component.css"]
})
export class OpleidingComponent implements OnInit {
  constructor() {}
  @Input() opleiding: Opleiding;

  ngOnInit(): void {}
}
