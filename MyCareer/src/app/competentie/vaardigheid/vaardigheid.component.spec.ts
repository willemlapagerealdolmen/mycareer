import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaardigheidComponent } from './vaardigheid.component';

describe('VaardigheidComponent', () => {
  let component: VaardigheidComponent;
  let fixture: ComponentFixture<VaardigheidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaardigheidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaardigheidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
