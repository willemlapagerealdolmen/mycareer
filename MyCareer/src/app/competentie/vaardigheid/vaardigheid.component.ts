import { Component, OnInit, Input, Pipe, PipeTransform } from "@angular/core";
import { Vaardigheid } from "src/app/models/vaardigheid.model";

@Component({
  selector: "app-vaardigheid",
  templateUrl: "./vaardigheid.component.html",
  styleUrls: ["./vaardigheid.component.css"]
})
export class VaardigheidComponent implements OnInit {
  constructor() {}
  @Input() vaardigheid: Vaardigheid;

  ngOnInit(): void {}
}
