import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject, of } from "rxjs";
import { delay, catchError, map } from "rxjs/operators";
import { environment } from "src/environments/environment.prod";
import { Ambitie } from "./models/ambitie.model";
import { Competentie } from "./models/competentie.model";
import { Vaardigheid } from "./models/vaardigheid.model";
import { Opleiding } from "./models/opleiding.model";
import { Event } from "./models/event.model";
import { Project } from "./models/project.model";
import { Functie } from "./models/functie.model";
import { Gebruiker } from "./models/gebruiker.model";

@Injectable({
  providedIn: "root"
})
export class DataService {
  public loadingError$ = new Subject<string>();

  constructor(private http: HttpClient) {}

  get ambities$(): Observable<Ambitie[]> {
    return this.http.get(`${environment.apiUrl}/user/ambitie`).pipe(
      catchError(error => {
        this.loadingError$.next(error.statusText);
        return of(null);
      }),
      map((list: any[]): Ambitie[] => list.map(Ambitie.fromJSON))
    );
  }

  //gebruiker

  get gebruiker$(): Observable<Gebruiker> {
    return this.http.get(`${environment.apiUrl}/user/`).pipe(
      catchError(error => {
        this.loadingError$.next(error.statusText);
        return of(null);
      }),
      map(Gebruiker.fromJSON)
    );
  }

  opleidingen$(isAmbitie: boolean): Observable<Opleiding[]> {
    return this.http
      .get(
        `${environment.apiUrl}/user/competentie/opleiding/${
          isAmbitie ? `ambitie` : `bezit`
        }`
      )
      .pipe(
        catchError(error => {
          this.loadingError$.next(error.statusText);
          return of(null);
        }),
        map((list: any[]): Opleiding[] => list.map(Opleiding.fromJSON))
      );
  }

  events$(isAmbitie: boolean): Observable<Event[]> {
    return this.http
      .get(
        `${environment.apiUrl}/user/competentie/event/${
          isAmbitie ? `ambitie` : `bezit`
        }`
      )
      .pipe(
        catchError(error => {
          this.loadingError$.next(error.statusText);
          return of(null);
        }),
        map((list: any[]): Event[] => list.map(Event.fromJSON))
      );
  }

  projecten$(isAmbitie: boolean): Observable<Project[]> {
    return this.http
      .get(
        `${environment.apiUrl}/user/competentie/project/${
          isAmbitie ? `ambitie` : `bezit`
        }`
      )
      .pipe(
        catchError(error => {
          this.loadingError$.next(error.statusText);
          return of(null);
        }),
        map((list: any[]): Project[] => list.map(Project.fromJSON))
      );
  }

  functies$(isAmbitie: boolean): Observable<Functie[]> {
    return this.http
      .get(
        `${environment.apiUrl}/user/competentie/functie/${
          isAmbitie ? `ambitie` : `bezit`
        }`
      )
      .pipe(
        catchError(error => {
          this.loadingError$.next(error.statusText);
          return of(null);
        }),
        map((list: any[]): Functie[] => list.map(Functie.fromJSON))
      );
  }

  vaardigheden$(isAmbitie: boolean): Observable<Vaardigheid[]> {
    return this.http
      .get(
        `${environment.apiUrl}/user/competentie/vaardigheid/${
          isAmbitie ? `ambitie` : `bezit`
        }`
      )
      .pipe(
        catchError(error => {
          this.loadingError$.next(error.statusText);
          return of(null);
        }),
        map((list: any[]): Vaardigheid[] => list.map(Vaardigheid.fromJSON))
      );
  }
}
