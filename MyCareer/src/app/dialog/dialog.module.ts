import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../material/material.module";
import { TaalDialogComponent } from "./taal-dialog/taal-dialog.component";

@NgModule({
  declarations: [TaalDialogComponent],
  imports: [CommonModule, MaterialModule],
  exports: [TaalDialogComponent]
})
export class DialogModule {}
