import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaalDialogComponent } from './taal-dialog.component';

describe('TaalDialogComponent', () => {
  let component: TaalDialogComponent;
  let fixture: ComponentFixture<TaalDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaalDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
