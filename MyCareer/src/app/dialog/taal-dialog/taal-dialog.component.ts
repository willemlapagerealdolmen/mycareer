import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-taal-dialog",
  templateUrl: "./taal-dialog.component.html",
  styleUrls: ["./taal-dialog.component.css"]
})
export class TaalDialogComponent implements OnInit {
  taal: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TaalDialogComponent>
  ) {}

  ngOnInit() {
    this.taal = this.fb.group({
      taal: ["", [Validators.required]],
      beheersing: ["", [Validators.required]]
    });
  }

  save() {
    this.dialogRef.close(this.taal.value);
  }

  close() {
    this.dialogRef.close();
  }

  getErrorMessage(errors: any) {
    if (!errors) {
      return null;
    }
    if (errors.required) {
      return "Gelieve dit in te vullen";
    }
  }
}
