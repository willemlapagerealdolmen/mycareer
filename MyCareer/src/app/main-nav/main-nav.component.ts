import { Component } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";

@Component({
  selector: "app-main-nav",
  templateUrl: "./main-nav.component.html",
  styleUrls: ["./main-nav.component.css"]
})
export class MainNavComponent {
  public isHandset$: boolean;

  /*
  //Breakpoints.Handset
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    */

  onResize(event) {
    this.isHandset$ = event.target.innerWidth <= 1250;
  }

  constructor(private breakpointObserver: BreakpointObserver) {}
}
