export interface AfspraakJson {
  id: number;
  beschrijving: string;
  datum: string;
}

export class Afspraak {
  constructor(
    private _id: number,
    private _beschrijving: string,
    private _datum: Date
  ) {}

  static fromJSON(json: AfspraakJson): Afspraak {
    const rec = new Afspraak(json.id, json.beschrijving, new Date(json.datum));
    return rec;
  }

  get id(): number {
    return this._id;
  }

  get beschrijving(): string {
    return this._beschrijving;
  }

  get datum(): Date {
    return this._datum;
  }
}
