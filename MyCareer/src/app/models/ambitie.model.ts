import { Doelstelling, DoelstellingJson } from "./doelstelling.model";
import { FunctieJson, Functie } from "./functie.model";
import { CompetentieJson, Competentie } from "./competentie.model";
import { Afspraak } from "./afspraak.model";

export interface AmbitieJson {
  id: number;
  titel: string;
  beschrijving: string;
  functie: FunctieJson;
  doelstellingen: DoelstellingJson[];
  benodigdeCompetenties: CompetentieJson[];
}

export class Ambitie {
  constructor(
    private _id: number,
    private _titel: string,
    private _beschrijving: string,
    private _functie: Functie,
    private _doelstellingen: Doelstelling[],
    private _competenties: Competentie[]
  ) {}

  static fromJSON(json: AmbitieJson): Ambitie {
    console.log(json);
    const amb = new Ambitie(
      json.id,
      json.titel,
      json.beschrijving,
      new Functie(
        json.functie.id,
        json.functie.naam,
        json.functie.beschrijving,
        new Date(json.functie.begin),
        new Date(json.functie.eind),
        json.functie.bedrijf,
        json.functie.nodigeCompetenties.map(Competentie.fromJSON)
      ),
      json.doelstellingen.map(Doelstelling.fromJSON),
      json.benodigdeCompetenties.map(Competentie.fromJSON)
    );
    return amb;
  }

  get id(): number {
    return this._id;
  }

  get titel(): string {
    return this._titel;
  }

  get beschrijving(): string {
    return this._beschrijving;
  }

  get functie(): Functie {
    return this._functie;
  }

  get doelstellingen(): Doelstelling[] {
    return this._doelstellingen;
  }

  get competenties(): Competentie[] {
    return this._competenties;
  }

  addDoelstelling(
    id?: number,
    beschrijving?: string,
    algemeen?: boolean,
    afspraken?: Array<Afspraak>
  ) {
    this._doelstellingen.push(
      new Doelstelling(id, beschrijving, algemeen, afspraken)
    );
  }

  addCompetentie(competentie?: Competentie) {
    this._competenties.push(competentie);
  }
}
