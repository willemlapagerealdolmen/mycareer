import { FunctionExpr } from "@angular/compiler";
import { Doelstelling } from "./doelstelling.model";
import { Afspraak } from "./afspraak.model";

export interface CompetentieJson {
  id: number;
  naam: string;
  beschrijving: string;
}

export class Competentie {
  constructor(
    private _id: number,
    private _naam: string,
    private _beschrijving: string
  ) {}

  static fromJSON(json: CompetentieJson): Competentie {
    const comp = new Competentie(json.id, json.naam, json.beschrijving);
    return comp;
  }

  get id(): number {
    return this._id;
  }

  get naam(): string {
    return this._naam;
  }

  get beschrijving(): string {
    return this._beschrijving;
  }
}
