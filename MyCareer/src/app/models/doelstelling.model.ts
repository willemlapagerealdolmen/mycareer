import { FunctionExpr } from "@angular/compiler";
import { Afspraak, AfspraakJson } from "./afspraak.model";

export interface DoelstellingJson {
  id: number;
  beschrijving: string;
  algemeen: boolean;
  afspraken: AfspraakJson[];
}
export class Doelstelling {
  constructor(
    private _id: number,
    private _beschrijving: string,
    private _algemeen: boolean,
    private _afspraken: Afspraak[]
  ) {}

  static fromJSON(json: DoelstellingJson): Doelstelling {
    const dlst = new Doelstelling(
      json.id,
      json.beschrijving,
      json.algemeen,
      json.afspraken.map(Afspraak.fromJSON)
    );
    return dlst;
  }

  get algemeen(): boolean {
    return this._algemeen;
  }

  get afspraken(): Afspraak[] {
    return this._afspraken;
  }

  get id(): number {
    return this._id;
  }

  get beschrijving(): string {
    return this._beschrijving;
  }

  addAfspraak(id: number, beschrijving?: string, datum?: Date) {
    this._afspraken.push(new Afspraak(id, beschrijving, datum));
  }
}
