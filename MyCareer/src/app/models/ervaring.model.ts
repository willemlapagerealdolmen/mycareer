import { Competentie } from './competentie.model';


export interface ErvaringJson {
  id: number;
  naam: string;
  beschrijving: string;
  begin: string;
  eind: string;
}

export class Ervaring extends Competentie{
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    private _begin: Date, 
    private _eind: Date
) {
    super(_id, _naam, _beschrijving);
  }

  static fromJSON(json: ErvaringJson): Ervaring {
    const erv = new Ervaring(
      json.id,
      json.naam,
      json.beschrijving,
      new Date(json.begin),
      new Date(json.eind)
    );
    return erv;
  }

  get begin(): Date {
    return this._begin;
  }

  get eind(): Date {
    return this._eind;
  }
  
}