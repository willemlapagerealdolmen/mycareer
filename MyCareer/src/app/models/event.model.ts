import { Ervaring } from './ervaring.model';


export interface EventJson {
  id: number;
  naam: string;
  beschrijving: string;
  begin: string;
  eind: string;
  organisator: string;
} 

export class Event extends Ervaring{
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    _begin: Date, 
    _eind: Date,
    private _organisator: string,
) {
    super(_id, _naam, _beschrijving, _begin, _eind);
  }

  static fromJSON(json: EventJson): Event {
    const pro = new Event(
      json.id,
      json.naam,
      json.beschrijving,
      new Date(json.begin),
      new Date(json.eind),
      json.organisator
    );
    return pro;
  }

  get organisator(): string {
    return this._organisator;
  }



}