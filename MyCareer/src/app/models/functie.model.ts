import { Competentie, CompetentieJson } from "./competentie.model";
import { Ervaring } from "./ervaring.model";

export interface FunctieJson {
  id: number;
  naam: string;
  beschrijving: string;
  begin: string;
  eind: string;
  bedrijf: string;
  nodigeCompetenties: CompetentieJson[];
}

export class Functie extends Ervaring {
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    _begin: Date,
    _eind: Date,
    private _bedrijf: string,
    private _competenties = new Array<Competentie>()
  ) {
    super(_id, _naam, _beschrijving, _begin, _eind);
  }

  static fromJSON(json: FunctieJson): Functie {
    const fun = new Functie(
      json.id,
      json.naam,
      json.beschrijving,
      new Date(json.begin),
      new Date(json.eind),
      json.bedrijf,
      json.nodigeCompetenties.map(Competentie.fromJSON)
    );
    return fun;
  }

  get bedrijf(): string {
    return this._bedrijf;
  }

  get competenties(): Competentie[] {
    return this._competenties;
  }

  addCompetentie(competentie?: Competentie) {
    this._competenties.push(competentie);
  }
}
