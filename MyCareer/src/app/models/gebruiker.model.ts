import { Competentie, CompetentieJson } from "./competentie.model";
import {
  Verantwoordelijke,
  VerantwoordelijkeJson
} from "./verantwoordelijke.model";
import { Review, ReviewJson } from "./review.model";
import { Ambitie, AmbitieJson } from "./ambitie.model";

export interface GebruikerJson {
  id: number;
  naam: string;
  voornaam: string;
  afdeling: string;
  verantwoordelijke: VerantwoordelijkeJson;
  zwakte: string[];
  hobby: string[];
  competenties: CompetentieJson[];
  ambities: AmbitieJson[];
}

export class Gebruiker {
  constructor(
    private _id: number,
    private _naam: string,
    private _voornaam: string,
    private _afdeling: string,
    private _verantwoordelijke: Verantwoordelijke,
    private _zwaktes = new Array<string>(),
    private _hobbys = new Array<string>(),
    private _competenties = new Array<Competentie>(),
    private _ambities = new Array<Ambitie>()
  ) {}

  static fromJSON(json: GebruikerJson): Gebruiker {
    const user = new Gebruiker(
      json.id,
      json.naam,
      json.voornaam,
      json.afdeling,
      null,
      /*
      new Verantwoordelijke(
          json.verantwoordelijke.id,
          json.verantwoordelijke.naam,
          new Array<string>(),
          new Array<string>(),          
          json.verantwoordelijke.voornaam,
          json.verantwoordelijke.afdeling,
          new Array<Competentie>(),
          new Array<Ambitie>(),
          new Array<Review>(),
          null, 
          new Array<Gebruiker>()
      ),*/
      json.zwakte,
      json.hobby,
      json.competenties.map(Competentie.fromJSON),
      json.ambities.map(Ambitie.fromJSON)
    );
    return user;
  }

  get id(): number {
    return this.id;
  }

  get competenties(): Competentie[] {
    return this._competenties;
  }

  get naam(): string {
    return this._naam;
  }

  get voornaam(): string {
    return this._voornaam;
  }
  get afdeling(): string {
    return this._afdeling;
  }
  get zwaktes(): string[] {
    return this._zwaktes;
  }
  get hobbys(): string[] {
    return this._hobbys;
  }

  get ambities(): Ambitie[] {
    return this._ambities;
  }

  get verantwoordelijke(): Verantwoordelijke {
    return this._verantwoordelijke;
  }

  addCompetentie(competentie?: Competentie) {
    this._competenties.push(competentie);
  }

  addAmbitie(ambitie?: Ambitie) {
    this._ambities.push(ambitie);
  }

  addHobby(hobby?: string) {
    this._hobbys.push(hobby);
  }

  addZwakte(zwakte?: string) {
    this._zwaktes.push(zwakte);
  }
}
