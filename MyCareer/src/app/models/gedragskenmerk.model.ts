import { FunctionExpr } from '@angular/compiler';
import { Project } from './project.model';
import { Verantwoordelijke } from './verantwoordelijke.model';
import { Gebruiker } from './gebruiker.model';
import { RealdolmenValue } from './realdolmenvalue.model';


export interface GedragskenmerkJson {
  naam: string;
  score: number;
  uitleg: string;
  voorbeeld: string;
} 

export class Gedragskenmerk extends RealdolmenValue {
  constructor(
    _naam: string,
    _score: number,
    _uitleg: string, 
    private _voorbeeld: string,
  ) {
    super(_naam, _score, _uitleg);
  }

  static fromJSON(json: GedragskenmerkJson): Gedragskenmerk {
    const gedr = new Gedragskenmerk(
      json.naam,
      json.score,
      json.uitleg,
      json.voorbeeld
    );
    return gedr;
  }

  get voorbeeld(): string {
    return this._voorbeeld;
  }
}