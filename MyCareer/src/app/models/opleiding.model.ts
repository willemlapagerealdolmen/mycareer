import { Ervaring } from "./ervaring.model";

export interface OpleidingJson {
  id: number;
  naam: string;
  beschrijving: string;
  begin: string;
  eind: string;
  onderwijsInstelling: string;
}

export class Opleiding extends Ervaring {
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    _begin: Date,
    _eind: Date,
    private _onderwijsinstelling: string
  ) {
    super(_id, _naam, _beschrijving, _begin, _eind);
  }

  static fromJSON(json: OpleidingJson): Opleiding {
    const opl = new Opleiding(
      json.id,
      json.naam,
      json.beschrijving,
      new Date(json.begin),
      new Date(json.eind),
      json.onderwijsInstelling
    );
    return opl;
  }

  get onderwijsinstelling(): string {
    return this._onderwijsinstelling;
  }
}
