import { Competentie, CompetentieJson } from "./competentie.model";
import { Ervaring } from "./ervaring.model";

export interface ProjectJson {
  id: number;
  naam: string;
  beschrijving: string;
  begin: string;
  eind: string;
  functie: string;
  projectleider: string;
  opdrachtgever: string;
  bedrijf: string;
  nodigeCompetenties: CompetentieJson[];
}

export class Project extends Ervaring {
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    _begin: Date,
    _eind: Date,
    private _functie: string,
    private _projectleider: string,
    private _opdrachtgever: string,
    private _bedrijf: string,
    private _competenties = new Array<Competentie>()
  ) {
    super(_id, _naam, _beschrijving, _begin, _eind);
  }

  static fromJSON(json: ProjectJson): Project {
    const proj = new Project(
      json.id,
      json.naam,
      json.bedrijf,
      new Date(json.begin),
      new Date(json.eind),
      json.functie,
      json.projectleider,
      json.opdrachtgever,
      json.bedrijf,
      json.nodigeCompetenties.map(Competentie.fromJSON)
    );
    return proj;
  }

  get bedrijf(): string {
    return this._bedrijf;
  }

  get functie(): string {
    return this._functie;
  }

  get projectleider(): string {
    return this._projectleider;
  }

  get opdrachtgever(): string {
    return this._opdrachtgever;
  }

  get competenties(): Competentie[] {
    return this._competenties;
  }

  addCompetentie(competentie?: Competentie) {
    this._competenties.push(competentie);
  }
}
