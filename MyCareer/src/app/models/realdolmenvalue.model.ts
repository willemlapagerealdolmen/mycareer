export interface RealdolmenValueJson {
  naam: string;
  score: number;
  uitleg: string;
} 

export class RealdolmenValue{
  constructor(
    private _naam: string, 
    private _score: number,
    private _uitleg: string 
) { }

  static fromJSON(json: RealdolmenValueJson): RealdolmenValue {
    const rdv = new RealdolmenValue(
      json.naam,
      json.score,
      json.uitleg
    );
    return rdv;
  }

  get naam(): string {
    return this._naam;
  }

  get score(): number {
    return this._score;
  }

  get uitleg(): string {
    return this._uitleg;
  }
}