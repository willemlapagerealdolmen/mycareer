import { Project, ProjectJson } from "./project.model";
import {
  Verantwoordelijke,
  VerantwoordelijkeJson
} from "./verantwoordelijke.model";
import { Gebruiker, GebruikerJson } from "./gebruiker.model";
import { RealdolmenValue, RealdolmenValueJson } from "./realdolmenvalue.model";
import { Gedragskenmerk, GedragskenmerkJson } from "./gedragskenmerk.model";
import { Competentie } from "./competentie.model";
import { AmbitieJson, Ambitie } from "./ambitie.model";

export interface ReviewJson {
  id: number;
  gebruikersId: number;
  sterktes: string[];
  werkpunten: string[];
  beschrijving: string;
  titel;
  string;
  potentieelbeschrijving: string;
  datum: string;
  project: ProjectJson;
  verantwoordelijke: VerantwoordelijkeJson;
  gebruiker: GebruikerJson;
  realdolmenValues: RealdolmenValueJson[];
  gedragskenmerken: GedragskenmerkJson[];
}

export class Review {
  constructor(
    private _id: number,
    private _gebruikersId: number,
    private _sterktes = new Array<string>(),
    private _werkpunten = new Array<string>(),
    private _beschrijving: string,
    private _titel: string,
    private _potentieelbeschrijving: string,
    private _datum: Date,
    private _project: Project,
    private _verantwoordelijke: Verantwoordelijke,
    private _gebruiker: Gebruiker,
    private _realdolmenValues = new Array<RealdolmenValue>(),
    private _gedragskenmerken = new Array<Gedragskenmerk>()
  ) {}

  static fromJSON(json: ReviewJson): Review {
    const rec = new Review(
      json.id,
      json.gebruikersId,
      json.sterktes,
      json.werkpunten,
      json.beschrijving,
      json.titel,
      json.potentieelbeschrijving,
      new Date(json.datum),
      new Project(
        json.project.id,
        json.project.naam,
        json.project.beschrijving,
        new Date(json.project.begin),
        new Date(json.project.eind),
        json.project.functie,
        json.project.projectleider,
        json.project.opdrachtgever,
        json.project.bedrijf,
        json.project.nodigeCompetenties.map(Competentie.fromJSON)
      ),
      new Verantwoordelijke(
        json.verantwoordelijke.id,
        json.verantwoordelijke.naam,
        new Array<string>(),
        new Array<string>(),
        json.verantwoordelijke.voornaam,
        json.verantwoordelijke.afdeling,
        new Array<Competentie>(),
        new Array<Ambitie>(),
        new Array<Review>(),
        null,
        new Array<Gebruiker>()
      ),
      null,
      /*
      new Gebruiker(
        json.gebruiker.id,
        json.gebruiker.naam,
        json.gebruiker.voornaam,
        json.gebruiker.afdeling,
        null,
        json.gebruiker.zwaktes,
        json.gebruiker.hobbys,
        new Array<Competentie>(),
        new Array<Ambitie>(),
        new Array<Review>()
      ),*/
      json.realdolmenValues.map(RealdolmenValue.fromJSON),
      json.gedragskenmerken.map(Gedragskenmerk.fromJSON)
    );
    return rec;
  }

  get id(): number {
    return this._id;
  }

  get beschrijving(): string {
    return this._beschrijving;
  }

  get titel(): string {
    return this._titel;
  }

  get potentieelbeschrijving(): string {
    return this._potentieelbeschrijving;
  }

  get sterktes(): string[] {
    return this._sterktes;
  }

  get werkpunten(): string[] {
    return this._werkpunten;
  }

  get project(): Project {
    return this._project;
  }

  get verantwoordelijke(): Verantwoordelijke {
    return this._verantwoordelijke;
  }

  get gebruiker(): Gebruiker {
    return this._gebruiker;
  }

  get datum(): Date {
    return this._datum;
  }

  get realdolmenvalues(): RealdolmenValue[] {
    return this._realdolmenValues;
  }

  get gedragskenmerken(): Gedragskenmerk[] {
    return this._gedragskenmerken;
  }

  addWerkpunt(werkpunt?: string) {
    this._werkpunten.push(werkpunt);
  }

  addSterkte(sterkte?: string) {
    this._sterktes.push(sterkte);
  }

  addRealdolmenValue(realdolmenvalue?: RealdolmenValue) {
    this._realdolmenValues.push(realdolmenvalue);
  }

  addGedragskenmerk(gedragskenmerk?: Gedragskenmerk) {
    this._gedragskenmerken.push(gedragskenmerk);
  }
}
