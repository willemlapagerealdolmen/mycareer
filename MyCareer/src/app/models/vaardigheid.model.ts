import { Competentie } from './competentie.model';

interface VaardigheidJson {
  id: number;
  naam: string;
  beschrijving: string;
  type: string;
  prioriteit: number;
} 

export class Vaardigheid extends Competentie{
  constructor(
    _id: number,
    _naam: string,
    _beschrijving: string,
    private _type: string, 
    private _prioriteit: number
) {
    super(_id, _naam, _beschrijving);
  }

  static fromJSON(json: VaardigheidJson): Vaardigheid {
    const vrdh = new Vaardigheid(
      json.id,
      json.naam,
      json.beschrijving,
      json.type,
      json.prioriteit
    );
    return vrdh;
  }

  get type(): string {
    return this._type;
  }

  get prioriteit(): number {
    return this._prioriteit;
  }
  
}