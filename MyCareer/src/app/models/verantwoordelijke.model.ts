import { Competentie, CompetentieJson } from "./competentie.model";
import { Gebruiker, GebruikerJson } from "./gebruiker.model";
import { Review, ReviewJson } from "./review.model";
import { AmbitieJson, Ambitie } from "./ambitie.model";

export interface VerantwoordelijkeJson {
  id: number;
  naam: string;
  zwakte: string[];
  hobby: string[];
  voornaam: string;
  afdeling: string;
  competenties: CompetentieJson[];
  ambities: AmbitieJson[];
  reviews: ReviewJson[];
  verantwoordelijke: VerantwoordelijkeJson;
  gebruikers: GebruikerJson[];
}

export class Verantwoordelijke extends Gebruiker {
  constructor(
    _id: number,
    _naam: string,
    _zwaktes = new Array<string>(),
    _hobbys = new Array<string>(),
    _voornaam: string,
    _afdeling: string,
    _competenties = new Array<Competentie>(),
    _ambities = new Array<Ambitie>(),
    _reviews = new Array<Review>(),
    _verantwoordelijke: Verantwoordelijke,
    private _gebruikers = new Array<Gebruiker>()
  ) {
    super(
      _id,
      _naam,
      _voornaam,
      _afdeling,
      _verantwoordelijke,
      _zwaktes,
      _hobbys,
      _competenties,
      _ambities
    );
  }

  static fromJSON(json: VerantwoordelijkeJson): Verantwoordelijke {
    const ver = new Verantwoordelijke(
      json.id,
      json.naam,
      json.zwakte,
      json.hobby,
      json.voornaam,
      json.afdeling,
      json.competenties.map(Competentie.fromJSON),
      json.ambities.map(Ambitie.fromJSON),
      json.reviews.map(Review.fromJSON),
      new Verantwoordelijke(
        json.verantwoordelijke.id,
        json.verantwoordelijke.naam,
        new Array<string>(),
        new Array<string>(),
        json.verantwoordelijke.voornaam,
        json.verantwoordelijke.afdeling,
        new Array<Competentie>(),
        new Array<Ambitie>(),
        new Array<Review>(),
        null,
        new Array<Gebruiker>()
      ),
      json.gebruikers.map(Gebruiker.fromJSON)
    );
    return ver;
  }

  get gebruikers(): Gebruiker[] {
    return this._gebruikers;
  }

  addGebruiker(gebruiker?: Gebruiker) {
    this._gebruikers.push(gebruiker);
  }
}
