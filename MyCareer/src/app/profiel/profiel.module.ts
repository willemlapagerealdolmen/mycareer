import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../material/material.module";
import { ProfileheaderComponent } from "./profileheader/profileheader.component";
import { ProfielComponent } from "./profiel/profiel.component";
import { AmbitieModule } from "../ambitie/ambitie.module";
import { CompetentieModule } from "../competentie/competentie.module";

@NgModule({
  declarations: [ProfielComponent, ProfileheaderComponent],
  imports: [CommonModule, MaterialModule, AmbitieModule, CompetentieModule],
  exports: [ProfielComponent]
})
export class ProfielModule {}
