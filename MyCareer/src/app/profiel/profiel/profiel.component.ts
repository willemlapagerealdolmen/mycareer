import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-profiel",
  templateUrl: "./profiel.component.html",
  styleUrls: ["./profiel.component.css"]
})
export class ProfielComponent implements OnInit {
  constructor() {}
  private isEdit: boolean = false;

  ngOnInit(): void {}

  toggleEdit() {
    this.isEdit = !this.isEdit;
  }
}
