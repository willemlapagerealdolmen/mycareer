import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Gebruiker } from "src/app/models/gebruiker.model";
import { DataService } from "src/app/data.service";

@Component({
  selector: "app-profileheader",
  templateUrl: "./profileheader.component.html",
  styleUrls: ["./profileheader.component.css"]
})
export class ProfileheaderComponent implements OnInit {
  constructor(private _dataService: DataService) {}
  public naam: String;
  public gebruiker$: Gebruiker;

  @Output() public isEdit: EventEmitter<boolean> = new EventEmitter<boolean>();

  ngOnInit(): void {
    this._dataService.gebruiker$.subscribe(g => {
      this.naam = g.voornaam + " " + g.naam;
    });
    this.isEdit.emit(false);
  }

  toggleEdit() {
    this.isEdit.emit(!this.isEdit);
  }
}
