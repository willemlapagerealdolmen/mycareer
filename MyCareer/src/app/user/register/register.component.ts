import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

function comparePasswords(control: AbstractControl): { [key: string]: any } {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');
  return password.value === confirmPassword.value
    ? null
    : { passwordsDiffer: true }; //return bij error
}

function serverSideValidateUsername(
  checkAvailabilityFn: (n: string) => Observable<boolean>
): ValidatorFn {
  return (control: AbstractControl): Observable<{ [key: string]: any }> => {
    return checkAvailabilityFn(control.value).pipe(
      map(available => {
        if (available) {
          return null;
        }
        return { userAlreadyExists: true };
      })
    );
  };
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user: FormGroup;
  public errorMsg: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.user = this.fb.group({
      afdeling: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: [
        '',
        [Validators.required, Validators.email]
        //,serverSideValidateUsername(this.authService.checkUserNameAvailability)
      ],
      passwordGroup: this.fb.group(
        {
          password: ['', [Validators.required, Validators.minLength(8)]],
          confirmPassword: ['', Validators.required]
        },
        { validator: comparePasswords }
      )
    });
  }

  getErrorMessage(errors: any) {
    if (!errors) {
      return null;
    }
    if (errors.required) {
      return 'Gelieve dit in te vullen';
    } else if (errors.minlength) {
      return `Er zijn minimum ${
        errors.minlength.requiredLength
      } karakter nodig (momenteel: ${errors.minlength.actualLength})`;
    } else if (errors.userAlreadyExists) {
      return `Deze gebruiker bestaat al`;
    } else if (errors.email) {
      return `Dit is geen geldig e-mailadres`;
     } else if (errors.passwordsDiffer) {
      return `De wachtwoorden zijn niet hetzelfde!`;
    } 
  }

  onSubmit() {
    this.authService
      .register(
        this.user.value.lastname,
        this.user.value.passwordGroup.password,
        this.user.value.firstname,
        this.user.value.afdeling,
        this.user.value.email
      )
      .subscribe(
        val => {
          if (val) {
            if (this.authService.redirectUrl) {
              this.router.navigateByUrl(this.authService.redirectUrl);
              this.authService.redirectUrl = undefined;
            } else {
              this.router.navigate(['/Ambitie']);
            }
          } else {
            this.errorMsg = `Kon niet inloggen`;
          }
        },
        (err: HttpErrorResponse) => {
          console.log(err);
            this.errorMsg =  `Er liep iets fout bij het inloggen`
        }
      );
  }

}
